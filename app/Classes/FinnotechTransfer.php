<?php


namespace App\Classes;

use App\Models\Transaction;
use App\Models\User;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class FinnotechTransfer
{
    protected $transferUrl;
    protected $sourceUser;
    protected $destinationUser;
    protected $trackId;
    protected $data;

    public function __construct($sourceUserId,$destinationUserId,$data)
    {
        $this->trackId = $trackId = Str::uuid();
        $this->sourceUser = User::find($sourceUserId);
        $this->destinationUser = User::find($destinationUserId);

        $this->transferUrl = 'https://sandboxapi.finnotech.ir/oak/v2/clients/'.$sourceUserId.'/transferTo/'.$trackId;

        $this->data = array(
            'destinationFirstname' => $this->destinationUser,
            'destinationLastname' => $this->destinationUser->last_name,
            'destinationNumber' => $this->destinationUser->destinationNumber,
            'sourceFirstName' => $this->sourceUser->firstt_name,
            'sourceLastName' => $this->sourceUser->last_name,
            'amount' => $data['amount'],
            'description' =>$data['description'],
            'paymentNumber' => $data['paymentNumber'],
            'reasonDescription' => $data['reasonDescription'],
            'deposit' => $data['deposit'],
            'secondPassword' => $data['secondPassword'],
        );
    }

    public function transfer(){
        $response = Http::post($this->transferUrl, $this->data);
        return $result = $response->json();
    }

    public function saveSuccessTransfer($result,$userId){

        $transaction = new Transaction($result);
        $transaction->userId = $userId;
        $transaction->amount = $result->amount;
        $transaction->description = $result->description;
        $transaction->destinationFirstname = $result->destinationFirstname;
        $transaction->destinationLastname = $result->destinationLastname;
        $transaction->destinationNumber = $result->destinationNumber;
        $transaction->inquiryDate = $result->inquiryDate;
        $transaction->inquirySequence = $result->inquirySequence;
        $transaction->inquiryTime = $result->inquiryTime;
        $transaction->message = $result->message;
        $transaction->paymentNumber = $result->paymentNumber;
        $transaction->refCode = $result->refCode;
        $transaction->sourceFirstname = $result->sourceFirstname;
        $transaction->sourceLastname = $result->sourceLastname;
        $transaction->sourceNumber = $result->sourceNumber;
        $transaction->type = $result->type === 'internal' ? 'داخلی' : 'انتقال وجه پایا';
        $transaction->reasonDescription = $result->reasonDescription;
        $transaction->trackId = $result->trackId;
        $transaction->save();
    }

    public function balance($result){

        $this->sourceUser->balance =  $this->sourceUser->balance - $result->amount;
        $this->sourceUser->save();

        $this->destinationUser->balance = $this->destinationUser->balance + $result->amount;
        $this->destinationUser->save();
    }

    public function saveFailedTransfer($result){

        $transaction = new Transaction();
        $transaction->userId = $this->sourceUser->id;
        $transaction->amount = $this->data['amount'];
        $transaction->description = $this->data['description'];
        $transaction->destinationFirstname = $this->destinationUser->first_name;
        $transaction->destinationLastname = $this->destinationUser->last_name;
        $transaction->destinationNumber = $this->data['destinationNumber'];
        $transaction->paymentNumber = $this->data['paymentNumber'];
        $transaction->sourceFirstname = $this->sourceUser->firstt_name;
        $transaction->sourceLastname = $this->sourceUser->last_name;
        $transaction->sourceNumber = $this->data['deposit'];
        $transaction->reasonDescription = $this->data['reasonDescription'];
        $transaction->trackId = $result->trackId;
        $transaction->save();

        return $result->trackId;
    }

}
