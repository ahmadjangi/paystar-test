<?php

namespace App\Filters\Admin;


use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class TransactionFilter
{

    public static function filter(Request $filters)
    {

        $query = static::applyDecoratorsFromRequest($filters, (new Transaction)->newQuery());
        return static::getResults($query,$filters);
    }

    private static function applyDecoratorsFromRequest(Request $request, Builder $query)
    {
        foreach ($request->all() as $filterName => $value) {
            $decorator = static::createFilterDecorator($filterName);
            if (static::isValidDecorator($decorator)) {
                $query = $decorator::apply($query, $value);
            }
        }
        return $query;
    }

    private static function createFilterDecorator($name)
    {
        return __NAMESPACE__ . '\\TransactionFilter\\' . studly_case($name);
    }

    private static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }

    private static function getResults(Builder $query,$filters)
    {
            return $query->paginate($filters->recordCount,
                    [
                        'id',
                        'name',
                        'sku',
                        'quantity',
                        'published'
                    ]);
    }
}
