<?php

namespace App\Filters\Admin\TransactionFilter;



use Illuminate\Database\Eloquent\Builder;

class TrackId implements Filter
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        return $value == '' ? $builder : $builder->where('track_id', '=',  $value );
    }
}
