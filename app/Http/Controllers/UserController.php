<?php

namespace App\Http\Controllers;

use App\Http\Requests\Transfer;
use App\Http\Requests\UpdateUserProfile;
use App\Services\UserService;

class UserController extends Controller
{
    protected $user;

    public function __construct(UserService $user)
    {
        $this->user = $user;
    }

    public function updateProfile(UpdateUserProfile $request, $id){

        $result = $this->user->updateProfile($request, $id);
        return apiResponse(null,'UserEditedSuccessful',
            $result === true ? 'constants.apiResponseCodes.success':'constants.apiResponseCodes.error');
    }

    public function transfer(Transfer $request){

        $result = $this->user->updateProfile($request);
        return apiResponse($result->trackId,$result->message,
            $result === true ? 'constants.apiResponseCodes.success':'constants.apiResponseCodes.error');
    }

    public function transactionList($id){

        $result = $this->user->transactionList($id);
        return apiResponse($result,'TransactionList',
            'constants.apiResponseCodes.success');
    }
}
