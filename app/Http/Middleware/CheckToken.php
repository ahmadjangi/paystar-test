<?php

namespace App\Http\Middleware;

use App\Models\Role;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Closure;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        AuthorizesRequests With Token
        if($request->header('Authorization') == 1234){

            return $next($request);
        }else{
            return apiResponse(null,'Unauthorized',
                'constants.apiResponseCodes.AuthorizationException',403);
        }
    }
}
