<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class Transfer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //صرفا بررسی لاگین کاربر
        if (Auth::check()){
            return true;
        }else{
            return false;
        }

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'amount'                => 'bail|required|numeric',
            'description'           => 'bail|required|max:30|string',
        ];

        if($this->request->bankType === 'Ayandeh'){
            $rules+= ['payment_number'=> 'bail|required|digits:30'];
            $rules+= ['reason_description' => 'bail|required|string|max:500'];
            return $rules;

        }elseif ($this->request->bankType === 'Parsian'){
            $rules+= ['second_password'       => 'bail|required',];
            return $rules;

        }elseif ($this->request->bankType === 'Keshavarzi'){
            return $rules;
        }

    }
}
