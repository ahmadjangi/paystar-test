<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'                    => 'bail|exists:users,id|required|integer',
            'first_name'            => 'bail|required|max:255|min:3|string',
            'last_name'             => 'bail|required|max:255|min:3|string',
            'bank_account_number'   => 'bail|required|digits:26',
        ];
    }
}
