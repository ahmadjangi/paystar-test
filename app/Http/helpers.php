<?php

use Illuminate\Support\Facades\Config;



if(!function_exists('apiResponse')){
    function apiResponse($data,$message,$code,$statusCode = 200){
        return response()->json([
            'data' => $data,
            'status' => [
                'message'=>__($message),
                'code'=>Config::get($code)],
        ],$statusCode);
    }
}
