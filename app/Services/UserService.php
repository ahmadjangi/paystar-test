<?php


namespace App\Services;


use App\Classes\FinnotechTransfer;
use App\Filters\Admin\TransactionFilter;
use App\Http\Requests\Transfer;
use App\Http\Requests\UpdateUserProfile;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class UserService
{
    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserProfile $request
     * @param int $id
     * @return JsonResponse
     */

    public static function updateProfile(UpdateUserProfile $request,$id){

        $user = User::find($id);
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->first_name = $request->input('bank_account_number');
        return $user->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserProfile $request
     * @param int $id
     * @return array
     */

    public static function transfer(Transfer $request){

        DB::beginTransaction();

        try {

            $sourceUserId = $request->sourceUserId;
            $destinationUserId =$request->destinationUserId;

            $data = array(
                'amount' => $request->amount,
                'description' => $request->description,
                'paymentNumber' => $request->paymentNumber,
                'reasonDescription' => $request->reasonDescription,
                'deposit' => $request->deposit,
                'secondPassword' => $request->secondPassword,
            );

            $transfer = new FinnotechTransfer($sourceUserId,$destinationUserId,$data);
            $result = $transfer->transfer();

            if($result->status ==="DONE"){

                $transfer->saveSuccessTransfer($result,$sourceUserId);
                $transfer->saveSuccessTransfer($result,$destinationUserId);
                $transfer->balance($result);

                DB::commit();

            }elseif($result->status === "FAILED"){

                $trackId = $transfer->saveFailedTransfer($result);

                DB::commit();

                return array('result'=>false,'trackId'=>$trackId,'message',$result->error->message);

            }elseif($result->status == "PENDING" || $result->status == "UNKNOWN"){
                //                فراخوانی سرویس استعلام Transfer Inquiry service
            }

        }catch (\Exception $e){
            DB::rollBack();

            //البته اگر انتقال وجه در API فینوتک با موفقیت انجام بشه
            //و با خطا در کدهای ما تراکنش rollback بشه باید پول منتقل شده
            //از حساب کاربر برداشت بشه که تو این مورد باید دوباره
// انتقال وجه معکوس بزنیم که زیاد جالب نیست حال نمیدونم فینوتک راهکاری داره یا نه

            return array('result'=>false,'trackId'=> null,'message',$e->getMessage());
        }
    }

    public static function transactionList(Transfer $request){
        return TransactionFilter::filter($request);
    }
}
