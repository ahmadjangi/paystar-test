<?php

return [
  'apiResponseCodes' => [
      'success' => '2000',
      'error' => '5000',
      'RequestError' => '4000',
      'ValidationException' => '4001',
      'MethodNotAllowedHttpException' => '4002',
      'AuthorizationException' => '4003',
      'NotFoundHttpException' => '4004',
      'GeneralError' => '4005',
      'QueryException' => '5001',
      'FatalErrorException' => '5002',
  ]
];
