<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('userId')->unsigned()->index();
            $table->bigInteger('amount');
            $table->string('description',30);
            $table->string('destination_first_name',33);
            $table->string('destination_number',33);
            $table->string('payment_number',30);
            $table->text('reason_description');
            $table->text('deposit');
            $table->string('source_first_name',30);
            $table->string('source_last_name',30);
            $table->string('second_password',255);
            $table->date('inquiry_date')->nullable();
            $table->time('inquiry_time')->nullable();
            $table->string('refCode',255)->nullable();
            $table->enum('type',['internal','paya']);
            $table->enum('status',['DONE','FAILED']);
            $table->string('trackId',40);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
